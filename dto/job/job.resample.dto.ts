export interface IJobJustResampledDto {
    id: string;
    audioFile: string;
}

export interface IJobResampleSavedDto {
    id: string;
    audioFile: string;
    configId: string;
}