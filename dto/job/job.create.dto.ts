export interface IJustCreatedJobDto {
    id: string;
    audioFile: string;
}

export interface IWillCreateJobDto {
    id: string;
    audioFile: string;
    configId: string;
}
