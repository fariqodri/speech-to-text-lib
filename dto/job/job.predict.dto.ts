export interface IJobPredictedDto {
    id: string;
    result: string;
    status: 'success' | 'failed';
}