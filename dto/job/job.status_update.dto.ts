export interface IJobStatusUpdateDto {
    id: string;
    status: 'pending' | 'processing' | 'failed' | 'success';
}