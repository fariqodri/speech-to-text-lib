export interface IUserConfigDeleteDto {
    id: string;
    userId: string;
}