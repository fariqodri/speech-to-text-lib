import { Accounts } from '../../data_types';

export interface IUserConfigCreateRequestBodyDto {
    userId: string;
    type: 'google'|'bahasakita';
    config: Accounts.IBahasaKitaAccount | Accounts.IGoogleServiceAccount;
}

export interface IUserConfigCreateMessageDto {
    id: string;
    userId: string;
    type: 'google' | 'bahasakita';
    config: Accounts.IBahasaKitaAccount | Accounts.IGoogleServiceAccount;
}