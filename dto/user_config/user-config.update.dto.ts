import { Accounts } from "../../data_types";


export interface IUserConfigUpdateRequestBodyDto {
    type: 'google'|'bahasakita';
    config: Accounts.IGoogleServiceAccount | Accounts.IBahasaKitaAccount;
}

export interface IUserConfigUpdateMessageDto {
    id: string;
    userId: string;
    type: 'google'|'bahasakita';
    config: Accounts.IGoogleServiceAccount | Accounts.IBahasaKitaAccount;
}