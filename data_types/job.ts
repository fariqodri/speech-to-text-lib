export interface IJob {
    id: string;
    configId: string;
    status: 'pending' | 'processing' | 'failed' | 'success';
    result: string | null;
    originalFileUrl: string | null;
    resampledFileUrl: string | null;
}
